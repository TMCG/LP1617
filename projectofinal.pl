:-include('SUDOKU').

%------------------------------------------------------------------------------
%               Projecto LP 2016/2017 - 2 Semestre LEIC-A
%                83567 - Tiago Miguel Calhanas Goncalves
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
%                         Propagacao de mudancas
%------------------------------------------------------------------------------
/*tira_num/4
    tira_num(Num, Puz, Posicoes, N_Puz) significa que N_Puz e' o resultante de
    de tirar o numero Num de todas as posicaoes em Posicoes do puzzle Puz.
*/
tira_num(Num, Puz, Pos, N_Puz):-
    percorre_muda_Puz(Puz, tira_num_aux(Num), Pos, N_Puz).

/*
tira_num_aux/4
  tira_num_aux(Num, Puz, Pos, N_Puz) significa que N_Puz e'o puzzle resultante
  de tirar o numero Num da posicao Pos do puzzle Puz.
*/
tira_num_aux(Num, Puz, Pos, N_Puz):-
    puzzle_ref(Puz, Pos, Cont),
    subtract(Cont, [Num], Subs),
    puzzle_muda_propaga(Puz, Pos, Subs, N_Puz).

/*
puzzle_muda_propaga/4
    puzzle_muda_propaga(Puz, Pos, Cont, N_Puz) faz o mesmo que o predicado
    puzzle_muda/4, mas, no caso de Cont ser uma lista unitaria, propaga a
    mudanca, isto e', retira o numero em Cont de todas as posicaoes relacionadas
    com Pos, isto e', todas as posicoes na mesma linha, coluna ou bloco.
*/
puzzle_muda_propaga(Puz, Pos, Cont, N_Puz):-
    length(Cont, N),
    N =\=1,
    puzzle_muda(Puz, Pos, Cont, N_Puz).
puzzle_muda_propaga(Puz, Pos, Cont, N_Puz):-
    length(Cont, N),
    N =:= 1,
    puzzle_muda(Puz, Pos, Cont, N_Puz2),
    posicoes_relacionadas(Pos, Posicoes),
    nth1(1, Cont, Num),
    membro(Num, Puz, Posicoes, FixedPos),
    tira_num(Num, N_Puz2, FixedPos, N_Puz).

/*membro/4
  membro(Num, Puz, Posicoes, FixedPos) verifica em que posicoes Posicoes e' do
  puzzle Puz que o Num pertence e deolve a lista dessas mesmasem FixedPos.
  */
membro(_, _, [], []).
membro(Num, Puz, [P|R], [P|I]):-
    puzzle_ref(Puz, P, Cont),
    member(Num, Cont),!,
    membro(Num, Puz, R, I).
membro(Num, Puz, [P|R], I):-
    puzzle_ref(Puz, P, Cont),
    \+member(Num, Cont),
    membro(Num, Puz, R, I).

%---------------------------------------------------------------------
%             Inicializacao do Puzzle
%---------------------------------------------------------------------
/*possibilidades/3
    possibilidades(Pos, Puz, Poss) significa que Poss e' a lista de numeros
    possiveis para a posicao Pos, do puzzle Puz.
*/
possibilidades(Pos, Puz, Poss):-
    puzzle_ref(Puz, Pos, Cont),
    length(Cont, N),
    N =\= 1,!,
    numeros(L),
    posicoes_relacionadas(Pos, Posicoes),
    conteudos_posicoes(Puz, Posicoes, Subs),
    remove_unitarias(L, Subs, Poss).
possibilidades(Pos, Puz, Poss):-
    puzzle_ref(Puz, Pos, Cont),
    length(Cont, N),
    N =:= 1,
    Poss = Cont.
/*remove_unitarias/2
  remove_unitarias(Poss, Cont) significa que Poss e'
*/
remove_unitarias(L, [], Poss):-!,
    Poss = L.
remove_unitarias(L, [P|R], Poss):-
    length(P,N),
    N =\= 1,!,
    remove_unitarias(L, R, Poss).
remove_unitarias(L, [P|R], Poss):-
    length(P, N),
    N =:= 1,
    subtract(L, P, L1),
    remove_unitarias(L1, R, Poss).

/*inicializa/2
  inicializa(Puz, N_Puz) significa que N_Puz e'o puzzle resultante de inicilizar
  o puzzle Puz.
*/

inicializa(Puz, N_Puz):-
    todas_posicoes(All),
    percorre_muda_Puz(Puz, inicializa_aux, All, N_Puz).
/*inicializa_aux/3
  inicializa_aux(Puz, Pos, N_Puz) significa que N_Puz e'o puzzle resultante de
  colocar na posicao Pos do puzzle Puz a lista com os numeros possiveis para
  essa posicao. Caso o conteudo da lista seja unitario nada acontece.
*/
inicializa_aux(Puz, Pos, N_Puz):-
    puzzle_ref(Puz, Pos, Cont),
    length(Cont, N),
    N =\= 1,
    possibilidades(Pos, Puz, Poss),
    puzzle_muda_propaga(Puz, Pos, Poss, N_Puz).
inicializa_aux(Puz, _, Puz).

%------------------------------------------------------------------------------
%                         Inspecao de Puzzles
%------------------------------------------------------------------------------
/*so_aparece_uma_vez/4
  so_aparece_uma_vez(Puz, Num, Posicoes, Pos_Num) significa que o numero Num
  so aparece numa das posicoes da lista Posicoes do puzzle Puz, e que essa
  posicao e' Pos_Num.
*/
so_aparece_uma_vez(Puz, Num, Posicoes, Pos_Num):-
  so_aparece_uma_vez_aux(Puz, Num, Posicoes, Pos_Num, 0).

/*so_aparece_uma_vez_aux/5
  so_aparece_uma_vez_aux(Puz, Num, Posicoes, Pos_Num, Contador) significa que
  o numero Num so aparece numa das posicoes da lista Posicoes do puzzle Puz,
  e que essa posicao e' Pos_Num.
*/
so_aparece_uma_vez_aux(_, _, [], _, 1):-!.
so_aparece_uma_vez_aux(Puz, Num, [P|R],  P , Contador):-
    puzzle_ref(Puz, P, Conteudo),
    member(Num, Conteudo),!,
    Contador1 is Contador + 1,
    so_aparece_uma_vez_aux(Puz, Num, R, P, Contador1).
so_aparece_uma_vez_aux(Puz, Num, [P|R], Pos_Num , Contador):-
    puzzle_ref(Puz, P, Conteudo),
    \+member(Num, Conteudo),
    so_aparece_uma_vez_aux(Puz, Num, R, Pos_Num , Contador).

/*inspecciona_num/4
  inspecciona_num(Posicoes, Puz, Num, N_Puz) significa que N_Puz e' o resultado
  de inspeccionar o grupo cujas posicoes sao Posicaoes, para o numero Num:
    - se Num so' ocorrer numa das posicoes de Posicoes e se o conteudo dessa
    posicao nao for uma lista unitaria, esse conteudo e' mudado para [Num] e
    esta mudanca e' propagada.
    - caso contrario, Puz = N_Puz.
*/
inspecciona_num(Posicoes, Puz, Num, N_Puz):-
    so_aparece_uma_vez(Puz, Num, Posicoes, Pos_Num),!,
    puzzle_muda_propaga(Puz, Pos_Num, [Num], N_Puz).
inspecciona_num(Posicoes, Puz, Num, Puz):-
    \+so_aparece_uma_vez(Puz, Num, Posicoes, _).

/*inspecciona_grupo/3
  inspecciona_grupo(Puz, Gr, N_Puz) inspecciona o grupo cujas posicoes sao as da
  lista Gr, do puzzle Puz para cada um dos numeros possiveis, sendo o resultado
  o puzzle N_Puz.
*/
inspecciona_grupo(Puz, Gr, N_Puz):-
    numeros(LNum),
    inspecciona_grupo_aux(Puz, Gr, N_Puz, LNum).

/*inspecciona_grupo_aux/4
  inspecciona_grupo(Puz, Gr, N_Puz, LNum) inspecciona o grupo cujas posicoes sao
  as da lista Gr, do puzzle Puz para cada um dos numeros possiveis, sendo o
  resultado o puzzle N_Puz.
*/
inspecciona_grupo_aux(N_Puz, _, N_Puz,[]):-!.
inspecciona_grupo_aux(Puz, Gr, N_Puz, [P|R]):-
    inspecciona_num(Gr, Puz, P, N_Puz1),
    inspecciona_grupo_aux(N_Puz1, Gr, N_Puz, R).

/*inspecciona/2
  inspecciona(Puz, N_Puz) inspecciona cada um dos grupos do puzzle Puz, para cada
  um dos numeros possiveis, sendo o resultado do puzzle N_Puz.
*/
inspecciona(Puz, N_Puz):-
    grupos(Gr),
    percorre_muda_Puz(Puz, inspecciona_grupo, Gr, N_Puz).

%------------------------------------------------------------------------------
%                            Verificacao de Solucoes
%------------------------------------------------------------------------------
/*grupo_correcto/3
  grupo_correcto(Puz, Nums, Gr) em que Puz e' um puzzle, significa que o grupo
  de Puz cujas poscisoes sao as da lista Gr esta correto, isto e', que contem
  todos os numeros da lista Nums, sem repeticoes.
*/
grupo_correcto(Puz, Nums, Gr):-
    conteudos_posicoes(Puz, Gr, LCont),
    verifica(Puz, LCont,Gr, Nums).

/*verfica/4
  verfica(Puz, LCont, Gr, L)
*/
verifica(_, [], _, []):-!.
verifica(Puz, LCont, Gr, [P|R]):-
    so_aparece_uma_vez(Puz, P, Gr, _),
    subtract(LCont, [[P]], LCont1),
    verifica(Puz, LCont1, Gr, R).

/*solucao/1
  solucao(Puz) significa que o puzzle Puz e' uma solucao, isto e', que todos os
  seus grupos contem todos os numeros possiveis, sem repeticoes.
*/
solucao(Puz):-
    numeros(LNum),
    grupos(Gr),
    solucao_aux(Puz, LNum, Gr).

solucao_aux(_, _, []):-!.
solucao_aux(Puz, LNum, [P|R]):-
    grupo_correcto(Puz, LNum, P),
    solucao_aux(Puz, LNum, R).

/*resolve/2
  resolve(Puz, Sol) significa que o Puzzle Sol e' uma solucao do Puzzle Puz.
*/
resolve(Puz, Puz):- /*Caso o puzzle ja seja uma solucao*/
    solucao(Puz).
resolve(Puz, Sol):-
    inicializa(Puz, Init),
    inspecciona(Init, Inspec),
    todas_posicoes(All),
    procura(Inspec, Sol, All, 1),
    solucao(Sol).

/*procura/4
  procura(Puz, Sol, Pos, N) significa que Sol e' o resultado de atraves de
  de hipotise completar o Puzzle Puz, que tem as posicoes Pos, e escolhendo o
  elemento de cada casa N.
*/
procura(Puz, Puz,Pos, _):- %*No caso em que ja nao existem casas unitarias e e'
    \+sel_n_unit(Puz, Pos, _).    %solucao do Puzzle.
procura(Puz, Sol, Pos, N):-
    sel_n_unit(Puz, Pos, N_Unit),
    seleciona_elemento(Puz, N_Unit, Elem, N),
    puzzle_muda_propaga(Puz, N_Unit, [Elem], N_Puz),
    cont_n_vazio(N_Puz, Pos),
    procura(N_Puz, Sol, Pos, 1).
procura(Puz, Sol, Pos, N):-
    sel_n_unit(Puz, Pos, N_Unit),
    seleciona_elemento(Puz, N_Unit, Elem, N),
    puzzle_muda_propaga(Puz, N_Unit, [Elem], N_Puz),
    \+cont_n_vazio(N_Puz, Pos),
    N1 is N+1,
    procura(Puz, Sol, Pos, N1).
procura(Puz, Sol, Pos, N):-
    sel_n_unit(Puz, Pos, N_Unit),
    seleciona_elemento(Puz, N_Unit, Elem, N),
    puzzle_muda_propaga(Puz, N_Unit, [Elem], N_Puz),
    cont_n_vazio(N_Puz, Pos),
    \+procura(N_Puz, Sol, Pos, 1),
    N1 is N+1,
    procura(Puz, Sol, Pos, N1).

/*cont_n_vazio/2
  cont_n_vazio(Puz, Pos) verifica se existem posicoes vazias no Puzzle Puz.
*/
cont_n_vazio(_, []):-!.
cont_n_vazio(Puz, [P|_]):-
    puzzle_ref(Puz, P, Cont),
    length(Cont, N),
    N =:= 0,!,
    fail.
cont_n_vazio(Puz, [_  |R]):-
    cont_n_vazio(Puz, R).

/*seleciona_elemento/4
  seleciona_elemento(Puz, Pos, Elem, N) seleciona o N elemento (Elem) da posicao
  Pos do Puzzle Puz.
*/
seleciona_elemento(Puz, Pos, Elem, N):-
    puzzle_ref(Puz, Pos, Cont),
    nth1(N, Cont, Elem).

/*sel_n_unit/3
  sel_n_unit(Puz, Pos, N_Unit) escolhe uma posicao nao unitaria das Posicoes Pos
  do Puzzle Puz.*/
sel_n_unit(_, [],_):-
    fail.
sel_n_unit(Puz, [P|R], N_Unit):-
    puzzle_ref(Puz, P, Cont),
    length(Cont, 1),!,
    sel_n_unit(Puz, R, N_Unit).
sel_n_unit(_, [P|_], P).
